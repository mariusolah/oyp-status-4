import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import HomeEnglish from '../views/HomeEnglish.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/home",
    name: "HomeEnglish",
    component: HomeEnglish
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
